package com.challenge.mocbackend.security.jwt;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;

public class JwtUser implements UserDetails {

    private final String id;
    private final String userName;
    private final String password;
    private final Date lastPasswordResetDate;
    private final boolean isActive;
    private final Collection<? extends GrantedAuthority> authorities;

    public JwtUser(String initId,
                   String initUserName,
                   String initPassword,
                   Date initLastPasswordResetDate,
                   Collection<? extends GrantedAuthority> initAuthorities,
                   boolean initIsActive) {
        id = initId;
        userName = initUserName;
        password = initPassword;
        lastPasswordResetDate = initLastPasswordResetDate;
        authorities = initAuthorities;
        isActive = initIsActive;
    }

    @JsonIgnore
    public String getId(){
        return id;
    }

    @JsonIgnore
    public Date getLastPasswordResetDate(){
        return lastPasswordResetDate;
    }

    @JsonIgnore
    @Override
    public String getUsername() {
        return userName;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

}
