package com.challenge.mocbackend.controller.users;

import com.challenge.mocbackend.dto.response.UserResponseDto;
import com.challenge.mocbackend.model.domain.User;
import com.challenge.mocbackend.model.service.RoomService;
import com.challenge.mocbackend.model.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.challenge.mocbackend.controller.users.ContentResponse.NO_CONTENT_RESPONSE;

@RestController
@RequestMapping
public class UserRestControllerV1 {

    @Autowired
    private RoomService roomService;

    @Autowired
    private UserService userService;

    @GetMapping(value = "users/{id}")
    public ResponseEntity<UserResponseDto> getUserById(
            @PathVariable(name = "id") String userId){

        User user = userService.findById(userId);

        if (user == null){
            return NO_CONTENT_RESPONSE;
        }

        UserResponseDto result = UserResponseDto.fromUser(user);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "users")
    public ResponseEntity<List<UserResponseDto>> getUsersList(){

        Set<User> userList = userService.getAllUsers();

        if (userList == null){
            return NO_CONTENT_RESPONSE;
        }

        List<UserResponseDto> result = userList.stream()
                .map(UserResponseDto::fromUser)
                .collect(Collectors.toList());

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
