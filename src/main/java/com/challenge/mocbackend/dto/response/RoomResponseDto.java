package com.challenge.mocbackend.dto.response;


import com.challenge.mocbackend.model.domain.Room;
import com.challenge.mocbackend.model.domain.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomResponseDto {

    private String id;
    private String creator_id;
    private Date createDate;
    private String roomName;
    private Set<User> users;

    public Room toModel(){

        Room room = new Room();
        room.setCreateDate(this.createDate);
        room.setCreator(this.creator);
        room.setId(this.id);
        room.setRoomName(this.roomName);
        room.setUsers(this.users);
        return room;
    }

    public static RoomResponseDto toDto(Room room){

        RoomResponseDto result = new RoomResponseDto();
        result.setCreateDate(room.getCreateDate());
        result.setCreator(room.getCreator());
        result.setCreator_id(room.getCreator().getId());
        result.setId(room.getId());
        result.setRoomName(room.getRoomName());
        result.setUsers(room.getUsers());
        return result;
    }

    @JsonIgnore
    private User creator;

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreator_id() {
        return creator_id;
    }

    public void setCreator_id(String creator_id) {
        this.creator_id = creator_id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

}
