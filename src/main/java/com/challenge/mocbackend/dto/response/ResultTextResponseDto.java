package com.challenge.mocbackend.dto.response;

public class ResultTextResponseDto {

    private String result;

    public static ResultTextResponseDto body(String body){
        ResultTextResponseDto r = new ResultTextResponseDto();
        r.setResult(body);
        return r;
    }
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


    public enum Message {
        USER_LEFT_ROOM(String );
        String text;

        Message(String text){
            this.text = text;
        }
    }
}
