package com.challenge.mocbackend.dto.response;

public class MessageDto {

    private String uder_id;
    private String room_id;
    private String message;

    public String getUder_id() {
        return uder_id;
    }

    public void setUder_id(String uder_id) {
        this.uder_id = uder_id;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
