package com.challenge.mocbackend.dto.requests;

public class UserIdRequestDto {

    private String user_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
