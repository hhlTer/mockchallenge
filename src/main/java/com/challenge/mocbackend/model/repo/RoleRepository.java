package com.challenge.mocbackend.model.repo;

import com.challenge.mocbackend.model.domain.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RoleRepository extends JpaRepository<UserRole, Long> {

    @Query(
            nativeQuery = true,
    value = "SELECT * FROM roles WHERE role_name =:rn")
    UserRole getRoleByName(@Param("rn") String roleName);
}
