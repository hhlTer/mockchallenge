package com.challenge.mocbackend.model.repo;

import com.challenge.mocbackend.model.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {

    public User findByUserName(String username);
}
