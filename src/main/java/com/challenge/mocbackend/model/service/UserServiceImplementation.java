package com.challenge.mocbackend.model.service;

import com.challenge.mocbackend.model.domain.Room;
import com.challenge.mocbackend.model.domain.User;
import com.challenge.mocbackend.model.domain.UserRole;
import com.challenge.mocbackend.model.enums.Roles;
import com.challenge.mocbackend.model.enums.Status;
import com.challenge.mocbackend.model.repo.RoleRepository;
import com.challenge.mocbackend.model.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImplementation implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder passwordEncoder;


    @Autowired
    public UserServiceImplementation(UserRepository initUserRepository, RoleRepository initRoleRepository, BCryptPasswordEncoder initBCryptPasswordEncoder){
        this.passwordEncoder = initBCryptPasswordEncoder;
        this.userRepository = initUserRepository;
        this.roleRepository = initRoleRepository;
    }

    @Transactional
    public void saveUser(User user){
        userRepository.save(user);
    }

    @Override
    public User register(User user){
        UserRole role = roleRepository.getRoleByName(Roles.ROLE_USER.name());
        Set<UserRole> roles = new HashSet<>();
        roles.add(role);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(roles);
        user.setStatus(Status.ACTIVE);
        User registeredUser = userRepository.save(user);

        return registeredUser;
    }

    @Override
    public Set<User> getAllUsers() {
        Set<User> result = new HashSet<>(userRepository.findAll());
        return result;
    }

    @Override
    public User findById(String userId) {
        User user = userRepository.findById(userId).orElse(null);
        if (user == null){
            throw new NullPointerException("user with id " + userId + " not found");
        }
        return user;
    }

    @Override
    @Transactional//TODO: test without Transactional annotation
    public User findByUserName(String username) {
        User user = userRepository.findByUserName(username);
        return user;
    }

}
