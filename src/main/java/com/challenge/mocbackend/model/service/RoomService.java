package com.challenge.mocbackend.model.service;

import com.challenge.mocbackend.model.domain.Room;

import java.util.List;
import java.util.Set;

public interface RoomService {
    Set<Room> getAll();
    Room findById(String roomId);
    Room createAndSaveRoom(String roomName, String userId);

    boolean validateRoomCreator(String user_id, String roomId);

    boolean deleteById(String roomId);

    Room saveRoom(Room room);
}
