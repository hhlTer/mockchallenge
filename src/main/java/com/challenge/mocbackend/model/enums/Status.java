package com.challenge.mocbackend.model.enums;

public enum  Status {
    ACTIVE, NON_ACTIVE
}
