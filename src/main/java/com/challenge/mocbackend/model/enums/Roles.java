package com.challenge.mocbackend.model.enums;

public enum Roles {
    ROLE_USER, ROLE_ADMIN
}
