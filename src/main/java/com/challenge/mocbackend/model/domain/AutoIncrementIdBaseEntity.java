package com.challenge.mocbackend.model.domain;


import javax.persistence.*;

@MappedSuperclass
public class AutoIncrementIdBaseEntity extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
