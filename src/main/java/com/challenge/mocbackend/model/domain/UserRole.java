package com.challenge.mocbackend.model.domain;



import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "roles")
public class UserRole extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "role_name")
    private String roleName;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    public long getId() {
        return id;
    }

    public void setId(long initId) {
        id = initId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String initRoleName) {
        roleName = initRoleName;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> initUsers) {
        users = initUsers;
    }

}
